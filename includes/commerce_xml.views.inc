<?php
/**
 * @file
 * Interface between commerce_xml.module and views.module.
 */

/**
 * Implementation of hook_views_plugins().
 */
function commerce_xml_views_plugins() {
  $path = drupal_get_path('module', 'commerce_xml') ;
  $views_path = drupal_get_path('module', 'views');
  return array(
    'module' => 'commerce_xml',
    'style' => array(
      'parent' => array(
  // this isn't really a display but is necessary so the file can
  // be included.
        'no ui' => TRUE,
        'handler' => 'views_plugin_style',
        'path' => "$views_path/plugins",
        'parent' => '',
  ),
      'commerce_xml' => array(
        'title' => t('commerce_xml'),
        'help' => t('Displays content as XML file for http://shop.i.ua/'),
        'handler' => 'commerce_xml_plugin_style',
        'path' => "$path/includes",
        'parent' => 'parent',
        'theme' => 'commerce_xml',
        'theme file' => 'theme.inc',
        'theme path' => "$path/theme",
        'uses row plugin' => False,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'feed',
  ),
  ),
  );
}