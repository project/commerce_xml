<?php
/**
 * @file
 * Theming functions and preprocessors
 */
/**
 * Generates XML file. Called by views system.
 * 
 * Function uses filter_xss($str, array()) not check_plain for output values in most cases, 
 * because we want to strip out html tags, not encode them.
 * @param $view
 * @param $options
 * @param $rows
 * @param $title
 * @return XML file
 */
function theme_commerce_xml($view, $options, $rows, $title) {

  $handler  = $view->style_plugin;
  $renders = $handler->render_fields($rows);
  drupal_set_header('Content-Type: text/html; charset='.$encoding);
  $output .= "<?xml version=\"1.0\" encoding=\"windows-1251\"?>\n";
  $output .= "<price date=\"" . date("Y-m-d h:i") . "\">\n";
  $output .= "  <name>" . filter_xss($options['shop']['name'], array()) . "</name>\n";
  $output .= "  <url>" . filter_xss($options['shop']['url'], array()) . "</url>\n\n";
  $output .= "  " . filter_xss($options['currencies'], array('currency')) . "\n";
  $all_regions = commerce_xml_regions();
  foreach ($options['shop']['region'] as $region_id) {
    $output .= "  <region>" . $all_regions[$region_id] . "</region>\n";
  }
  
  if ($options['vocabulary']) {
    $output .= "  <catalog>\n";
    $categories=taxonomy_get_tree($options['vocabulary']);
    foreach ($categories as $c) {
      $output .= "    <category id=\"" . $c->tid . "\"";
      if ($c->parents[0]!=0) $output .= " parentId=\"" . $c->parents[0] . "\"";
      $output .= ">" . _commerce_xml_cleanstr($c->name) . "</category>\n";
    }
    $output .= "  </catalog>\n\n";
  }
  $output .= "  <items>\n\n";
  foreach ($rows as $num => $row) {
    $output .= "    <item ";
    foreach (commerce_xml_offerattributes() as $id) {
      $v=_commerce_xml_getattribute($id, $options, $renders[$num]);
      if ($v!==FALSE) $output .= $id . "=\"" . _commerce_xml_cleanstr($v) . "\" ";
    }
    $output .= ">\n";
      if($attr = _commerce_xml_getattribute("name", $options, $renders[$num])) {
        $output .= "      <name>" . _commerce_xml_cleanstr($attr) . "</name>\n";
      }
      if($attr = _commerce_xml_getattribute('categoryId', $options, $renders[$num])) {
        $output .= "      <categoryId>" . _commerce_xml_cleanstr($attr) . "</categoryId>\n";
      }
      if($attr = _commerce_xml_getattribute("price", $options, $renders[$num])) {
        $output .= "      <price>" . _commerce_xml_cleanstr($attr) . "</price>\n";
      }
      if($attr = _commerce_xml_getattribute("url", $options, $renders[$num])) {
        $output .= "      <url>" . _commerce_xml_cleanstr($attr) . "</url>\n";
      }
      if($attr = _commerce_xml_getattribute("image", $options, $renders[$num])) {
        $output .= "      <image>" . _commerce_xml_cleanstr($attr) . "</image>\n";
      }
      if($attr = _commerce_xml_getattribute("vendor", $options, $renders[$num])) {
        $output .= "      <vendor>" . _commerce_xml_cleanstr($attr) . "</vendor>\n";
      }
      if($attr = _commerce_xml_getattribute("description", $options, $renders[$num])) {
        $output .= "      <description>" . _commerce_xml_cleanstr($attr, array()) . "</description>\n";
      }    
    $output .= "    </item>\n\n";
  }
  $output .= "  </items>\n";
  $output .= "</price>\n";
  return iconv('utf-8', 'cp1251', $output);;
}

/**
 * Search for value for given tag if in defferent places
 * @param $id
 *   name of tag
 * @param $options
 *   array of views options
 * @param $renderedrow
 *   array of rendered views data row
 *
 * @return
 * 	 Rendered value for given $id. FALSE if it shouldn't be showed.
 */
function _commerce_xml_getattribute($id, $options, $renderedrow) {
  if ($options['yacolumns'][$id]['field']=='') {
    return FALSE;
  }
  elseif ($options['yacolumns'][$id]['field']=='--static--') {
    return $options['yacolumns'][$id]['static'];
  }
  else {
    return $renderedrow[$options['yacolumns'][$id]['field']];
  }
}

function _commerce_xml_cleanstr($str) {
  return htmlspecialchars(filter_xss($str, array()), ENT_QUOTES);
}
